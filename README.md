<!-- (c) https://github.com/MontiCore/monticore -->
# struct_msgs
Contains the Messages **ba_struct_Point.msg** and **ba_struct_Rectangle.msg** generated from **Point.struct** and **Rectangle.struct**. Used by the **emam_converter** to send the vehicle hulls to the model. 
